package semenov;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.IntPredicate;
import java.util.function.IntSupplier;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class Streams<T> {

    public Collection<T> filterCollection(Collection<T> collection, Predicate<T> predicate) {
        return collection.stream()
                .filter(predicate)
                .collect(Collectors.toCollection(ArrayList::new));
    }

    public void generateTenNumbersWithZeroRemainder(int divider) {
        AtomicInteger start = new AtomicInteger();
        IntSupplier intSupplier = () -> start.getAndIncrement();
        IntPredicate predicate = (i -> i % divider == 0);

        IntStream.generate(intSupplier)
                .filter(predicate)
                .limit(10)
                .forEach(System.out::println);
    }

    public void printSortedArray(String[] array) {
        Comparator<String> comparator = (s1, s2) -> {
            if (s1.length() < s2.length()) {
                return -1;
            }
            if (s1.length() > s2.length()) {
                return 1;
            }
            return s1.compareTo(s2);
        };

        Arrays.stream(array)
                .sorted(comparator)
                .forEach(System.out::println);
    }

    public Map<Integer, Long> groupArrayByLengthWithCount(String[] array) {
        return Arrays.stream(array).
                collect(Collectors.groupingBy(
                        String::length, Collectors.counting()));
    }
}
