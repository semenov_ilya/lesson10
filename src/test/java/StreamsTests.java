import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import semenov.Streams;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.*;
import java.util.function.Function;
import java.util.function.Predicate;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class StreamsTests {
    private final ByteArrayOutputStream out = new ByteArrayOutputStream();

    private Streams<String> stringStreams = new Streams<>();
    private Collection<String> collection = Arrays.asList("Wer", "reitet", "so", "spat", "durch"
            , "Nacht", "und", "Wind");
    private Predicate<String> predicate;
    private Function<Integer, Integer> function;

    @BeforeEach
    private void catchOutput() {
        System.setOut(new PrintStream(out));
    }

    @AfterEach
    private void resetOutput() {
        out.reset();
    }

    /**
     * Тест проверяет фильтрацию коллекции по входящему на вход предикату
     */
    @Test
    public void filterCollectionTest() {
        predicate = s -> s.endsWith("t");
        Collection expected = Arrays.asList("reitet", "spat", "Nacht");
        Collection actual = stringStreams.filterCollection(collection, predicate);

        assertEquals(expected, actual);


        predicate = s -> s.length() <= 3;
        expected = Arrays.asList("Wer", "so", "und");
        actual = stringStreams.filterCollection(collection, predicate);

        assertEquals(expected, actual);
    }

    /**
     * Тест проверяет вывод 10 чисел, дающие нулевой остаток при делении
     */
    @Test
    public void generateTenNumbersWithZeroRemainderTest1() {
        final int divider = 27;
        stringStreams.generateTenNumbersWithZeroRemainder(divider);

        String output = out.toString();
        String[] outputArray = output.split("[\r]\n");

        assertEquals(10, outputArray.length);

        function = i -> i % divider;
        long sumOfRemainders = Arrays.stream(outputArray)
                .map(Integer::valueOf)
                .map(function)
                .reduce(0, Integer::sum);

        assertEquals(0, sumOfRemainders);
    }

    /**
     * Тест проверяет вывод 10 чисел, дающие нулевой остаток при делении
     */
    @Test
    public void generateTenNumbersWithZeroRemainderTest2() {
        final int divider = 16;
        stringStreams.generateTenNumbersWithZeroRemainder(divider);
        String[] outputArray = getOutputArray();

        assertEquals(10, outputArray.length);

        function = i -> i % divider;
        int sumOfRemainders = Arrays.stream(outputArray)
                .map(Integer::valueOf)
                .map(function)
                .reduce(0, Integer::sum);

        assertEquals(0, sumOfRemainders);
    }

    /**
     * Тест проверяет вывод упорядоченного перечня эелеметов массива
     */
    @Test
    public void printSortedArrayTest() {
        List expected = Arrays.asList("so", "Wer", "und", "Wind", "spat", "Nacht", "durch", "reitet");

        String[] inputArray = collection.stream().toArray(String[]::new);
        stringStreams.printSortedArray(inputArray);

        String[] actual = getOutputArray();

        assertEquals(expected, Arrays.asList(actual));
    }


    /**
     * Тест проверяет группировку массива строка по длине и преобразование в мэп с количеством
     */
    @Test
    public void groupArrayByLengthWithCountTest() {
        Map<Integer, Long> map = Map.of(
                2, 1L,
                3, 2L,
                4, 2L,
                5, 2L,
                6, 1L);
        TreeMap<Integer, Long> expected = new TreeMap<>(map);

        String[] inputArray = collection.stream().toArray(String[]::new);
        Map<Integer, Long> actual = stringStreams.groupArrayByLengthWithCount(inputArray);

        assertEquals(expected.size(), actual.size());
        assertEquals(expected.keySet(), actual.keySet());
        assertEquals(expected.toString(), actual.toString());
    }

    private String[] getOutputArray() {
        String output = out.toString();
        return output.split("[\r]\n");
    }

}
